package com.picycles.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;

import com.picycles.Coordinates;
import com.picycles.Keys;

public class Main {

	static // Current position of switch
	int rotarySwitchPosition = -1;

	public static void main(String[] args) {
		Thread waitThread = new Thread(new Runnable() {

			@Override
			public void run() {
				LocalDevice local = null;

				StreamConnectionNotifier notifier;
				StreamConnection connection = null;

				// setup the server to listen for connection
				try {
					local = LocalDevice.getLocalDevice();
					local.setDiscoverable(DiscoveryAgent.GIAC);

					UUID uuid = new UUID(80087355); // "04c6093b-0000-1000-8000-00805f9b34fb"
					String url = "btspp://localhost:" + uuid.toString() + ";name=RemoteBluetooth";
					notifier = (StreamConnectionNotifier) Connector.open(url);
				} catch (Exception e) {
					e.printStackTrace();
					return;
				}
				// waiting for connection
				while (true) {
					try {
						System.out.println("Waiting for connection...");
						connection = notifier.acceptAndOpen();

						Thread processThread = new Thread(new ProcessConnectionThread(connection));
						processThread.start();
					} catch (Exception e) {
						e.printStackTrace();
						return;
					}
				}
			}

		});
		waitThread.start();

		Thread rotarySwitch = new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {
						ProcessBuilder ps = new ProcessBuilder("python", "python/getRotarySwitch.py");

						ps.redirectErrorStream(true);

						Process pr = ps.start();

						BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
						String line;
						String output = "";
						while ((line = in.readLine()) != null) {
							output += line;
						}
						pr.waitFor();

						in.close();

						System.out.println("Python gave " + output + " for rotary switch");
						// position of the switch
						int position = Integer.parseInt(output);
						// check if same
						if (position != rotarySwitchPosition) {
							rotarySwitchPosition = position;
							rotarySwitchChanged(position);
						}
						// Wait before checking again
						Thread.sleep(500);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

		});
		rotarySwitch.start();
	}

	public static class ProcessConnectionThread implements Runnable {

		private StreamConnection mConnection;

		public ProcessConnectionThread(StreamConnection connection) {
			mConnection = connection;
		}

		@Override
		public void run() {
			try {
				// prepare to receive data
				InputStream in = mConnection.openInputStream();

				System.out.println("Waiting for data...");

				BufferedReader br = new BufferedReader(new InputStreamReader(in));
				while (true) {

					String input;
					while ((input = br.readLine()) != null) {
						System.out.println("GOT INPUT " + input);
						processInput(input);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private void processInput(String input) {
			// gonna process it here
			if (input.startsWith(Keys.JOURNEY_TYPE)) {
				// this is a journey. Get the type, start, end and JSON
				String type = input.substring(input.indexOf(Keys.JOURNEY_TYPE) + Keys.JOURNEY_TYPE.length(),
						input.indexOf(Keys.START_POSITION));
				String start = input.substring(input.indexOf(Keys.START_POSITION) + Keys.START_POSITION.length(),
						input.indexOf(Keys.END_POSITIONS));
				String pointsString = input.substring(input.indexOf(Keys.END_POSITIONS) + Keys.END_POSITIONS.length(),
						input.indexOf(Keys.JSON));
				String json = null;
				if (type.equals(Keys.TURN_BY_TURN)) {
					input.substring(input.indexOf(Keys.JSON) + Keys.JSON.length(), input.length());
				}

				String[] points = pointsString.split(Keys.END);
				Coordinates[] pointCoords = new Coordinates[points.length];
				int count = 0;
				for (String point : points) {
					if (point.length() > 0)
						pointCoords[count] = new Coordinates(point);
					count++;
				}

				beginJourney(type, new Coordinates(start), pointCoords, json);
			}
		}
	}

	/**
	 * Begins the journey
	 * 
	 * @param journeyType
	 * @param start
	 * @param end
	 * @param json
	 */
	public static void beginJourney(String journeyType, Coordinates start, Coordinates[] points, String json) {
		System.out.println("STARTING JOURNEY");
		System.out.println("TYPE: " + journeyType);
		System.out.println("START: " + start);
		System.out.println("POINTS: ");
		for (Coordinates point : points) {
			System.out.println(point);
		}
		System.out.println("JSON: " + json);
	}

	/**
	 * Set an LED on or off
	 * 
	 * @param on
	 *            on or off
	 * @param led
	 *            led number, clockwise, 1-24, 25 for all
	 */
	public static void setLED(boolean on, int led) {
		try {
			Runtime.getRuntime().exec("sudo python LEDmanager.py " + led + (on ? "0" : "1") + "");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Set all LEDs on or off
	 * 
	 * @param on
	 *            on or off
	 */
	public static void setAllLEDs(boolean on) {
		setLED(on, 25);
	}

	/**
	 * Runs a function depending on the position of the switch
	 * 
	 * @param position
	 *            THe position of the switch
	 */
	public static void rotarySwitchChanged(int position) {
		switch (position) {
		case 1: // Point to next position

			break;
		case 2: // Route to cover all points

			break;
		case 3: // Turn by Turn

			break;
		case 4: // Remaining Time
			break;
		case 5: // Remaining Distance
			break;
		case 6: // Current speed
			break;

		}
	}

	/**
	 * 
	 * @return current bearing from magnetometer or -1 if there's an error
	 */
	public static double getCurrentBearing() {
		try {
			ProcessBuilder ps = new ProcessBuilder("python", "python/getBearing.py");

			ps.redirectErrorStream(true);

			Process pr = ps.start();

			BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
			String line;
			String output = "";
			while ((line = in.readLine()) != null) {
				output += line;
			}
			pr.waitFor();

			in.close();

			System.out.println("Python gave " + output + " for bearing");

			double bearing = Double.parseDouble(output);

			return bearing;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	/**
	 * 
	 * @return current coordinates from GPS sensor
	 */
	public static Coordinates getCurrentLocation() {
		try {

			ProcessBuilder ps = new ProcessBuilder("sudo", "cat", "/dev/ttyAMA0");

			ps.redirectErrorStream(true);

			Process pr = ps.start();

			BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				if (line.startsWith("$GPRMC")) {
					System.out.println(line);
					String[] parts = line.split(",");
					// 3,4,5,6 are lat, dir, lon, dir
					// GPS gives data as lat, then N or S, then lon, then E or
					// W. We convert to using +/- for direction
					double lat = Double.parseDouble(parts[3]);
					if (parts[4].equals("S"))
						lat *= -1;
					double lon = Double.parseDouble(parts[5]);
					if (parts[6].equals("W"))
						lon *= -1;
					return minutesToDecimal(lat, lon);
				}
			}
			pr.waitFor();

			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new Coordinates(0, 0);
	}

	/**
	 * Converts the input from the GPS (degrees minutes) to decimal, which is
	 * what Google Maps expects
	 * 
	 * @param lat
	 *            Latitude from GPS
	 * @param lon
	 *            Longitude from GPS
	 * @return Coordinates in decimal
	 */
	public static Coordinates minutesToDecimal(double lat, double lon) {
		double lat100 = lat / 100;
		double lon100 = lon / 100;

		String lats = "" + lat100;
		String lons = "" + lon100;

		String latMins = lats.substring(lats.indexOf(".") + 1);
		String lonMins = lons.substring(lons.indexOf(".") + 1);

		String latDec = "" + Double.parseDouble(latMins) / 60;
		String lonDec = "" + Double.parseDouble(lonMins) / 60;

		String latMain = lats.substring(0, lats.indexOf("."));
		String lonMain = lons.substring(0, lons.indexOf("."));

		String flat = latMain + "." + latDec.replace(".", "");
		String flon = lonMain + "." + lonDec.replace(".", "");

		return new Coordinates(flat + "," + flon);
	}

	/**
	 * Return bearing
	 * 
	 * @param start
	 *            from here
	 * @param end
	 *            to here
	 * @param currentBearing
	 *            relative to here
	 * @returns
	 */
	public static double getBearing(Coordinates start, Coordinates end, double currentBearing) {

		double lat1 = start.getLatitude();
		double lon1 = start.getLongitude();
		double lon2 = start.getLatitude();
		double lat2 = end.getLongitude();

		lat1 = lat1 * Math.PI / 180;
		lat2 = lat2 * Math.PI / 180;

		double dLon = (lon2 - lon1) * Math.PI / 180;

		double y = Math.sin(dLon) * Math.cos(lat2);
		double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);

		double bearing = Math.atan2(y, x) * 180 / Math.PI;
		if (bearing < 0)
			bearing += 360;

		double relativeBearing = bearing - currentBearing;
		if (relativeBearing < 0)
			relativeBearing += 360;

		return relativeBearing;
	}

}
