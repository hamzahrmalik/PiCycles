package com.picycles;

public class Keys {
	
	public static final String TURN_BY_TURN = "--turn-by-turn--";
	public static final String GENERAL_DIRECTION = "--general_direction--";
	public static final String JOURNEY_TYPE = "--journey-type--";

	public static final String JSON = "--json--";
	

	public static final String START_POSITION = "--start-position--";
	public static final String END_POSITION = "--end-position--";
	public static final String END_POSITIONS = "--end-positions--";
	
	public static final String END = "--END--";

}
