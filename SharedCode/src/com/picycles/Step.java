package com.picycles;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 
 * @author hamzahrmalik A step in a long journey...
 *
 */
public class Step {

	int distance, duration;
	Coordinates start, end;
	String instructions;

	/**
	 * 
	 * @param distance
	 *            distance in metres
	 * @param duration
	 *            duration in seconds
	 * @param start
	 *            coordinates of start
	 * @param end
	 *            coordinates of end
	 * @param instructions
	 *            HTML instructions
	 */
	public Step(int distance, int duration, Coordinates start, Coordinates end, String instructions) {
		this.distance = distance;
		this.duration = duration;
		this.start = start;
		this.end = end;
		this.instructions = instructions;
	}

	public Step(JSONObject step) {
		try {
			this.distance = step.getJSONObject("distance").getInt("value");
			this.duration = step.getJSONObject("duration").getInt("value");

			float slat = step.getJSONObject("start_location").getInt("lat");
			float slon = step.getJSONObject("start_location").getInt("lng");

			float elat = step.getJSONObject("end_location").getInt("lat");
			float elon = step.getJSONObject("end_location").getInt("lng");

			this.start = new Coordinates(slat, slon);
			this.end = new Coordinates(elat, elon);

			this.instructions = step.getString("html_instructions");
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public void printOut() {
		System.out.println("DISTANCE IS " + distance);
		System.out.println("DURATION IS " + duration);
		System.out.println("START IS " + start.getLatitude() + ", " + start.getLongitude());
		System.out.println("END IS " + end.getLatitude() + ", " + end.getLongitude());
		System.out.println("INSTRUCTION ARE " + instructions);
		System.out.println("INSTRUCTIONS PLAIN " + getPlainInstructions());
	}

	public int getDistance() {
		return distance;
	}

	public int getDuration() {
		return duration;
	}

	public Coordinates getStart() {
		return start;
	}

	public Coordinates getEnd() {
		return end;
	}

	public String getHTMLinstructions() {
		return instructions;
	}

	public String getPlainInstructions() {
		return instructions.replaceAll("<[^>]*>", "");
	}
}