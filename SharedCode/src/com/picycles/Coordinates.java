package com.picycles;

/**
 * 
 * @author hamzahrmalik
 * 
 *         Holds a longitude and latitude
 *
 */
public class Coordinates {

	private double lon, lat;

	public Coordinates(double lat, double lon) {
		this.lon = lon;
		this.lat = lat;
	}

	public Coordinates(String coords) {
		String[] parts = coords.split(",");
		try {
			lat = Double.parseDouble(parts[0]);
			lon = Double.parseDouble(parts[1]);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	public double getLongitude() {
		return lon;
	}

	public double getLatitude() {
		return lat;
	}

	@Override
	public String toString() {
		return lat + "," + lon;
	}

}