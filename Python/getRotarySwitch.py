import RPi.GPIO as GPIO
from time import sleep

GPIO.setmode(GPIO.BOARD)


pos1 = 7
pos2 = 12
pos3 = 18
pos4 = 16
pos5 = 11
pos6 = 15

GPIO.setup(pos1, GPIO.IN)
GPIO.setup(pos2, GPIO.IN)
GPIO.setup(pos3, GPIO.IN)
GPIO.setup(pos4, GPIO.IN)
GPIO.setup(pos5, GPIO.IN)
GPIO.setup(pos6, GPIO.IN)

if(GPIO.input(pos1)):
    print "1"
elif(GPIO.input(pos2)):
    print "2"
elif(GPIO.input(pos3)):
    print "3"
elif(GPIO.input(pos4)):
    print "4"
elif(GPIO.input(pos5)):
    print "5"
elif(GPIO.input(pos6)):
    print "6"

GPIO.cleanup()
