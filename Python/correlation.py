from math import sqrt
from math import atan
from math import degrees
from math import radians
from math import atan2
from math import sin
from math import cos
from operator import itemgetter

curveCutoff = 0.9

def integeriser (thing):
    i=0
    while i<len(thing):
        thing[i] = int(thing[i])
        i=i+1
    return(thing)

def Ex (things):
    #print(things)
    total = 0
    for i in things:
        total = total+int(i)
   # print("Ex has "+ str(total))
    return total

def Ex2 (things):
    total = 0
    for i in things:
        total = total+int(i)*int(i)
    return total

def Exy (xthings,ythings):
    #print ("in Exy xthings is " + str(xthings) + " and  y things is " + str(ythings))
    x=0
    total = 0
    while x<len(xthings):
        total = total + (int(xthings[x])*int(ythings[x]))
        x= x+1
    #print ("Exy has " +str(total))
    return total

def sxx (things):
    return Ex2(things)-((Ex(things)*Ex(things))/len(things))

def syy (things):
    return(sxx(things))

def sxy (xthings,ythings):
    #print ("sxy has"+ str(Exy(xthings,ythings)-((Ex(xthings)*Ex(ythings))/len(xthings))))
    return Exy(xthings,ythings)-((Ex(xthings)*Ex(ythings))/len(xthings))
    

def r (xthings,ythings):
    r= abs(sxy(xthings,ythings)/(sqrt(sxx(xthings)*syy(ythings))))
    return r

def b (xthings, ythings):
    return sxy(xthings,ythings)/sxx(xthings)

def a (xthings,ythings):
    return (Ex(ythings)/len(xthings))-(b(xthings, ythings)*(Ex(xthings)/len(xthings)))

def incCurrent (things,currentThing):
    AndCurrent = list(things)
    x=0
    while x<(len(things)):   
        AndCurrent.append(currentThing)
        x= x+1
    return AndCurrent

def angleFromX (current,xthings,ythings):
    #print ("angleFromX recieved current as " +str(current)+" xthings as "+str(xthings) + " and ythings as "+str(ythings))
    m = b(incCurrent(xthings,current[0]),incCurrent(ythings,current[1]))
    print("m = "+ str(m))
    angle = abs(degrees(atan(m)))
    print (angle)
    return(angle)

def distance (current,xValue,yValue):
    return sqrt(((xValue-current[0])**2)+((yValue-current[1])**2))

def cutoff (current, xthings, ythings, curveCutoff):
    global nextOne
    checkIfPassedNext(current,xthings,ythings)
    i = nextOne+1
    currentConsiderationX = []
    currentConsiderationY = []
    currentConsiderationX.append(xthings[i-1])
    currentConsiderationY.append(ythings[i-1])
    cutoffNow = False
    while i<len(xthings) and cutoffNow==False:
        #print ("check if "+str(distance(current, xthings[i], ythings[i]))+" > "+str(distance(current, xthings[i-1], ythings[i-1])))
        if distance(current, xthings[i], ythings[i])>distance(current, xthings[i-1], ythings[i-1]):
            currentConsiderationX.append(xthings[i])
            currentConsiderationY.append(ythings[i])
            #print("Is "+str(r(incCurrent(currentConsiderationX,current[0]),incCurrent(currentConsiderationY,current[1])))+" below " + str(curveCutoff) + "?")
            if r(incCurrent(currentConsiderationX,current[0]),incCurrent(currentConsiderationY,current[1]))<curveCutoff:
                cutoffNow = True
                #print("Yes")
                currentConsiderationX.pop(len(currentConsiderationX)-1)
                currentConsiderationY.pop(len(currentConsiderationY)-1)
        else:
            cutoffNow = True
            print("cutoff due to further point being closer")
        i = i+1
    return currentConsiderationX, currentConsiderationY

def checkIfPassedNext (current,xthings,ythings):
    global nextOne
    direction = setGeneral(current,xthings,ythings)
    print (direction)
    if direction == 0:
        if current[0]>xthings[nextOne] and current[1]>ythings[nextOne]:
            nextOne = nextOne + 1
    elif direction == 1:
        if current[0]>xthings[nextOne] and current[1]<ythings[nextOne]:
            nextOne = nextOne + 1
    elif direction == 2:
        if current[0]<xthings[nextOne] and current[1]<ythings[nextOne]:
            nextOne = nextOne + 1
    else:
        if current[0]<xthings[nextOne] and current[1]>ythings[nextOne]:
            nextOne = nextOne + 1
        
def correctBearing(bearing, currentBear):
    adjBearing = bearing - currentBear
    if adjBearing <0:
        adjBearing = adjBearing+360
    return adjBearing

def setGeneral (current, xthings, ythings):
    global nextOne
    if xthings[nextOne]>current[0]:
        if ythings[nextOne]>current[1]:
            direction = 0
        else:
            direction = 1
    else:
        if ythings[nextOne]<current[1]:
            direction = 2
        else:
            direction = 3
    return direction




#-------------------Call me-----------------------------------------------#

def routingBearing (current, xlist, ylist, currentBear):
    global curveCutoff
    angle = angleFromX(current, *cutoff(current,xlist,ylist,curveCutoff))
    direction = setGeneral(current,xlist,ylist)
    if direction == 0:
        bearing = 90-angle
    elif direction == 1:
        bearing = 90 +angle
    elif direction == 2:
        bearing = 270-angle
    else:
        bearing = 270+angle
    return correctBearing(bearing, currentBear)

def pointToPoint (lat1, lon1, lat2, lon2, currentBear):
    lat1 = radians(lat1)
    lat2 = radians(lon2)
    dLon = radians(lon2-lon1)
    y= sin(dLon)*cos(lat2)
    x=cos(lat1)*sin(lat2) - sin(lat1)*cos(lat2)*cos(dLon)
    bearing = radians(atan2(y, x))
    if bearing<0:
        bearing = bearing+360
    bearing = correctBearing(bearing, currentBear)
    print (bearing)
    return bearing

#-------------------------------Outputs-----------------------------------#

def light (bearing, bloom):
    #leds = [34,12,52,243]
    ledsOnAmount = []
    for i in leds:
        ledsOnAmount.append(0)

if input("DEBUG (0/1) - ") == 1:    
    current = integeriser(input ("Current coords: ").split(","))
    xlist = integeriser(input ("xcoords, seperate with comma (no spaces)").split(","))
    ylist = integeriser(input ("ycoords (x"+str(len(xlist))+"), seperate with comma (no spaces)").split(","))
    nextOne=int(input("DEBUG ONLY: Set nextOne"))
    currentBear = int(input("DEBUG ONLY: Set currentBear"))
    routingBearing(current,xlist,ylist, currentBear)
else:
    nextOne = 0























#----------------------------------------Unused Code--------------------------------------------------#


#def cutoff (current, xthings, ythings, curveCutoff):
    #xywithDists = list()
    #i = 0
    #while i <len(xthings):
        #xywithDists.append([xthings[i],ythings[i],distance(current,xthings[i],ythings[i])])
        #i=i+1
    #print (xywithDists)
    #xywithDists = sorted(xywithDists, key=itemgetter(2))
    #tbreturned = list()
    #tbreturned.append(xywithDists[0])
    #j = 1
    #pmcc = 1
    #while pmcc>curveCutoff and j<len(xywithDists):
        #tbreturned.append(xywithDists[j])
        #print ("tbreturned is " + str(tbreturned))
        #pmcc = r(recover(tbreturned,0),recover(tbreturned,1))
        #print ("so pmcc is " + str(pmcc))
        #j=j+1
    #return tbreturned

#def recover (sortedcombined,pos):
    #recovered = list()
    #i = 0
    #while i<len(sortedcombined):
        #temp = (sortedcombined[i])
        #recovered.append(temp[pos])
        #i = i+1
    #return recovered

#print (r(xlist,ylist))
#print ("y="+str(a(xlist,ylist))+"+"+str(b(xlist,ylist))+"x")
#angleFromX(current,xlist,ylist)
#print (current)
#print(cutoff(current,xlist,ylist,curveCutoff))
#angleFromX(current, *cutoff(current,xlist,ylist,curveCutoff))

