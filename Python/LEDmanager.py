import sys

#Takes two arguments
#First is LED number, 1-24 meaning that LED and 25 meaning all
#Second is 0 for on or 1 for off

#Constants
ON = 0
OFF = 1
ALL = 25

#Turns on/off specified LED
def turnOn(ledNumber, state):
    if(state == ON):
        print("Turning on " + str(ledNumber))
    elif(state == OFF):
        print("Turning off " + str(ledNumber))

#The LED number
ledNumber = int(sys.argv[1])
print("LED NUMBER IS " + str(ledNumber))
#The state (0 or 1)
state = int(sys.argv[2])
print("State IS " + str(state))

if(ledNumber == 25):
    loop = 1
    while(loop <= 25):
        turnOn(loop, state)
        loop += 1
elif(ledNumber < 25 and ledNumber > 0):
    turnOn(ledNumber, state)
else:
    print("LED NUMBER OF #" + str(ledNumber) + "# is invalid")
