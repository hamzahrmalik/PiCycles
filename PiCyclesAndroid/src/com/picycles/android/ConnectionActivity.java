package com.picycles.android;

import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * 
 * @author hamzahrmalik
 * 
 *         Manages the bluetooth connection
 *
 */
public class ConnectionActivity extends AppCompatActivity {

	private BluetoothAdapter btAdapter = null;
	private BluetoothSocket btSocket = null;
	private OutputStream outStream = null;

	private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

	// server mac address. Currently hardcoded to Hamzah's Mac's MAC ;) //TODO
	// add searching and pairing
	private static String address = "78:9F:70:7A:B3:4B";

	boolean isConnected = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		btAdapter = BluetoothAdapter.getDefaultAdapter();
	}

	@Override
	public void onResume() {
		super.onResume();

		onSearchBegun();
		final BluetoothDevice device = btAdapter.getRemoteDevice(address);

		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				while (!isConnected) {
					try {
						btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
						btAdapter.cancelDiscovery();
						btSocket.connect();
						outStream = btSocket.getOutputStream();
						onConnectionEstablished();
						isConnected = true;
						Thread.sleep(2000);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

		});
		t.start();

	}

	@Override
	public void onPause() {
		super.onPause();
		isConnected = false;
		try {
			if (outStream != null)
				outStream.flush();
			btSocket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Called when search for device begins
	 */
	public void onSearchBegun() {

	}

	/**
	 * Called when connection has been established
	 */
	public void onConnectionEstablished() {

	}

	/**
	 * Send a message to server
	 * 
	 * @param message
	 *            message to send
	 */
	public void sendMessage(String message) {
		byte[] msgBuffer = (message + "\n").getBytes();
		try {
			outStream.write(msgBuffer);
			outStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
