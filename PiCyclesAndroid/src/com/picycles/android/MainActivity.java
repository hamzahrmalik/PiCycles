package com.picycles.android;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ImageView;

/**
 * 
 * @author hamzahrmalik This activity will show the GUI for connecting to the
 *         PiCycle
 *
 */
public class MainActivity extends ConnectionActivity {

	ImageView connectionStatus;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		connectionStatus = (ImageView) findViewById(R.id.connectionStatus);
	}

	@Override
	public void onSearchBegun() {
		System.out.println("SEARCH BEGUN");
	}

	@Override
	public void onConnectionEstablished() {
		this.runOnUiThread(new Runnable(){

			@Override
			public void run() {
				connectionStatus.setImageResource(R.drawable.green);
			}
			
		});
	}

	public void map(View v) {
		Intent i = new Intent(this, MapActivity.class);
		startActivity(i);
	}
	
	public void stats(View v) {
		
	}
	
	public void settings(View v) {
		Intent i = new Intent(this, Settings.class);
		startActivity(i);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);

		return super.onCreateOptionsMenu(menu);
	}

}