package com.picycles.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import android.content.Context;

/**
 * 
 * @author hamzahrmalik
 * 
 *         This class contains useful methods
 *
 */
public class Util {

	/**
	 * 
	 * @author hamzahrmalik This class holds a name-value pair for a HTTP POST
	 *         request
	 *
	 */
	static class BasicNameValuePair {

		String key, value;

		public BasicNameValuePair(String key, String value) {
			this.key = key;
			this.value = value;
		}

		public String getKey() {
			return key;
		}

		public String getValue() {
			return value;
		}
	}

	/**
	 * Make a HTTP GET request
	 * 
	 * @param c
	 *            a Context
	 * @param URL
	 *            a URL for the request
	 * @param params
	 *            A list of {@link #BasicNameValuePair}
	 * @return The result of the post
	 * @throws Exception
	 */
	public static String HTTPrequest(Context c, String URL, List<BasicNameValuePair> params) throws Exception {
		String result = "";

		String toWrite = getQuery(params);

		URL += toWrite;

		URL url = new URL(URL);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setReadTimeout(10000);
		conn.setConnectTimeout(10000);
		conn.setRequestMethod("POST");
		conn.setDoInput(true);
		conn.setDoOutput(true);

		conn.connect();
		result = inputStreamToString(conn.getInputStream());
		conn.disconnect();

		return result;
	}

	/**
	 * Converts the {@link #BasicNameValuePair} into a URL format
	 * 
	 * @param params
	 *            The parameters
	 * @return the String to be sent to HTTP
	 * @throws UnsupportedEncodingException
	 */
	public static String getQuery(List<BasicNameValuePair> params) throws UnsupportedEncodingException {
		StringBuilder result = new StringBuilder();
		boolean first = true;

		for (BasicNameValuePair pair : params) {
			if (first)
				first = false;
			else
				result.append("&");

			result.append(URLEncoder.encode(pair.getKey(), "UTF-8"));
			result.append("=");
			result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
		}

		return result.toString();
	}

	public static String inputStreamToString(InputStream is) {
		String line = "";
		StringBuilder total = new StringBuilder();
		BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		try {
			while ((line = rd.readLine()) != null) {
				total.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return total.toString();
	}

}
