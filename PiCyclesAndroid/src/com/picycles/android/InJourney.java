package com.picycles.android;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import com.picycles.Coordinates;
import com.picycles.Keys;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

/**
 * 
 * @author hamzahrmalik This activity is displayed during the journey. Shows
 *         stats
 *
 */
public class InJourney extends ConnectionActivity {
	// Box shown to user
	TextView textBox;
	// Type of journey, turn by turn or general direction
	String journeyType;
	// JSON for turn by turn steps
	String json;
	// Start and end
	Coordinates start, end;
	Coordinates[] points;
	// addresses
	String startAd, endAd;
	String pointAds;

	boolean hasBegun = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_in_journey);

		textBox = (TextView) findViewById(R.id.textbox);
		// Get the intent and the journey type from it
		Intent i = getIntent();
		journeyType = i.getStringExtra(Keys.JOURNEY_TYPE);
		if (journeyType.equals(Keys.TURN_BY_TURN))
			json = i.getStringExtra(Keys.JSON);
		// get start and end
		start = new Coordinates(i.getStringExtra(Keys.START_POSITION));
		String[] points = i.getStringArrayExtra(Keys.END_POSITIONS);
		this.points = new Coordinates[points.length];
		// get coordinates for each point
		int count = 0;
		for (String point : points) {
			this.points[count] = new Coordinates(point);
			count++;
		}
		end = this.points[this.points.length - 1];
		// get addresses
		try {
			Geocoder geocoder = new Geocoder(this, Locale.getDefault());
			List<Address> startAds = geocoder.getFromLocation(start.getLatitude(), start.getLongitude(), 1);
			List<Address> endAds = geocoder.getFromLocation(end.getLatitude(), end.getLongitude(), 1);

			pointAds = "<b>via</b><br>";
			count = 0;
			for (Coordinates c : this.points) {
				if (count == this.points.length - 1)
					break;
				Address ad = geocoder.getFromLocation(c.getLatitude(), c.getLongitude(), 1).get(0);
				pointAds += ad.getAddressLine(0) + "\n";
				count++;
			}

			startAd = startAds.get(0).getAddressLine(0) + " " + startAds.get(0).getAddressLine(1);
			endAd = endAds.get(0).getAddressLine(0) + " " + endAds.get(0).getAddressLine(1);

		} catch (IOException e) {
			e.printStackTrace();
		}
		// display information
		textBox.setText(Html.fromHtml("<font size='5'><b>Journeying from</b><br>" + startAd + "<br><b>to</b><br>" + endAd
				+ "<br>" + pointAds + "<br><br><b>Journey Type</b>: "
				+ (journeyType.equals(Keys.TURN_BY_TURN) ? "Turn By Turn" : "General Direction")));

	}

	@Override
	public void onConnectionEstablished() {
		if (!hasBegun) {
			String endPoints = "";
			for (Coordinates coords : points) {
				endPoints += coords.toString() + Keys.END;
			}
			sendMessage(Keys.JOURNEY_TYPE + journeyType + Keys.START_POSITION + start + Keys.END_POSITIONS + endPoints
					+ Keys.JSON + ((json == null) ? "" : json));
		}
	}

}
