package com.picycles.android;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.picycles.Coordinates;
import com.picycles.Keys;
import com.picycles.Step;
import com.picycles.android.Util.BasicNameValuePair;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnCloseListener;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

/**
 * 
 * @author hamzahrmalik Shows user a map to choose journey
 *
 */
public class MapActivity extends ConnectionActivity implements OnMapReadyCallback {

	boolean hasSetStart = false;
	Marker startPoint;
	ArrayList<Marker> points;
	GoogleMap map;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);

		points = new ArrayList<Marker>();

		checkLocationServices();

		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);

		getSupportActionBar().setSubtitle("Select journey end point");
	}

	/**
	 * Checks if location services are enabled on the phone. Redirects user to
	 * enable them if they're not
	 */
	public void checkLocationServices() {
		LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			Toast.makeText(this, "Please enable location services", Toast.LENGTH_LONG).show();
			Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(intent);
		}
	}

	@Override
	public void onMapReady(final GoogleMap map) {
		this.map = map;
		GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
			@Override
			public void onMyLocationChange(Location location) {
				if (startPoint != null) {
					startPoint.remove();
				}
				// Get my position
				LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
				// Add the marker
				startPoint = map.addMarker(new MarkerOptions().position(loc).title("Starting Position"));
				// Zoom to it if not already
				if (!hasSetStart) {
					zoomToPoint(loc);
					hasSetStart = true;
				}

			}
		};
		map.setMyLocationEnabled(true);
		map.setOnMyLocationChangeListener(myLocationChangeListener);
		map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

			@Override
			public void onMapClick(LatLng point) {
				putMarker(point);
			}
		});
	}

	/**
	 * Zooms into the point
	 * 
	 * @param point
	 *            Coordinates of point
	 */
	public void zoomToPoint(LatLng point) {

		CameraUpdate center = CameraUpdateFactory.newLatLng(point);
		CameraUpdate zoom = CameraUpdateFactory.zoomTo(13);

		map.moveCamera(center);
		map.animateCamera(zoom);
	}

	/**
	 * Puts end marker at this point
	 * 
	 * @param point
	 */
	public void putMarker(LatLng point) {
		MarkerOptions opt = new MarkerOptions().position(point);
		opt.title("" + (points.size() + 1));
		opt.draggable(true);
		points.add(map.addMarker(opt));
	}

	/**
	 * Calculates the journey directions
	 */
	public void start() {
		if (startPoint == null) {
			Toast.makeText(this, "Couldn't detect where you are. Check Location Services are available",
					Toast.LENGTH_LONG).show();
			return;
		}
		if (points.size() < 1) {
			Toast.makeText(this, "Please choose at least one destination first", Toast.LENGTH_SHORT).show();
			return;
		}
		/*
		 * try { // Get address of start and end points Geocoder geocoder = new
		 * Geocoder(this, Locale.getDefault()); List<Address> start =
		 * geocoder.getFromLocation(startPoint.getPosition().latitude,
		 * startPoint.getPosition().longitude, 1); List<Address> end =
		 * geocoder.getFromLocation(points.get(points.size() -
		 * 1).getPosition().latitude, points.get(points.size() -
		 * 1).getPosition().longitude, 1); String addresses = "via\n"; int count
		 * = 0; for (Marker m : points) { if (count == points.size() - 1) break;
		 * Address ad = geocoder.getFromLocation(m.getPosition().latitude,
		 * m.getPosition().longitude, 1).get(0); addresses +=
		 * ad.getAddressLine(0) + "\n"; count++; }
		 * 
		 * String startAd = start.get(0).getAddressLine(0) + " " +
		 * start.get(0).getAddressLine(1); String endAd =
		 * end.get(0).getAddressLine(0) + " " + end.get(0).getAddressLine(1);
		 * 
		 * // ask if they want general direction or turn by turn
		 * 
		 * AlertDialog.Builder builder = new AlertDialog.Builder(this);
		 * builder.setTitle(
		 * "Would you like turn-by-turn directions or would you like the general direction?"
		 * ); builder.setMessage("Journeying from \n\n" + startAd + "\n to \n" +
		 * endAd + "\n" + addresses); builder.setPositiveButton(
		 * "General Direction", new OnClickListener() {
		 * 
		 * @Override public void onClick(DialogInterface arg0, int arg1) {
		 * generalDirection(); }
		 * 
		 * }); builder.setNegativeButton("Turn-by-Turn", new OnClickListener() {
		 * 
		 * @Override public void onClick(DialogInterface arg0, int arg1) {
		 * turnByTurn(); }
		 * 
		 * }); builder.setNeutralButton("Cancel", null); builder.show();
		 *
		 * } catch (IOException e) { e.printStackTrace(); }
		 */
		// Now chosen by rotary switch so..
		generalDirection();
	}

	/**
	 * Starts general directions navigation
	 */
	public void generalDirection() {
		Intent i = new Intent(this, InJourney.class);
		i.putExtra(Keys.JOURNEY_TYPE, Keys.GENERAL_DIRECTION);

		i.putExtra(Keys.START_POSITION,
				new Coordinates(startPoint.getPosition().latitude, startPoint.getPosition().longitude).toString());
		String[] coordinates = new String[points.size()];
		int count = 0;
		for (Marker m : points) {
			coordinates[count] = new Coordinates(m.getPosition().latitude, m.getPosition().longitude).toString();
			count++;
		}
		i.putExtra(Keys.END_POSITIONS, coordinates);
		startActivity(i);
	}

	/**
	 * Starts turn by turn navigation
	 */
	public void turnByTurn() {
		// Network not allowed on main thread
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
					params.add(new BasicNameValuePair("origin", "125 queens drive mossley hill l18 1jl"));
					params.add(new BasicNameValuePair("destination", "London"));
					params.add(new BasicNameValuePair("travel_mode", "BICYCLING"));
					final String result = Util.HTTPrequest(MapActivity.this,
							"http://maps.googleapis.com/maps/api/directions/json?", params);
					System.out.println(result);
					parseResult(result);
					MapActivity.this.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							beginTurnByTurn(result);
						}

					});
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		});
		t.start();
	}

	/**
	 * Begins navigation. Sends the JSON to the InJourney activity
	 * 
	 * @param json
	 *            JSON from google
	 */
	public void beginTurnByTurn(String json) {
		Intent i = new Intent(this, InJourney.class);
		i.putExtra(Keys.JOURNEY_TYPE, Keys.TURN_BY_TURN);
		i.putExtra(Keys.JSON, json);

		i.putExtra(Keys.START_POSITION,
				new Coordinates(startPoint.getPosition().latitude, startPoint.getPosition().longitude).toString());
		String[] coordinates = new String[points.size()];
		int count = 0;
		for (Marker m : points) {
			coordinates[count] = new Coordinates(m.getPosition().latitude, m.getPosition().longitude).toString();
			count++;
		}
		i.putExtra(Keys.END_POSITIONS, coordinates);
		startActivity(i);
	}

	/**
	 * Parses result from Google API request for directions
	 * 
	 * @param result
	 *            The JSON from Google
	 */
	public void parseResult(String result) {
		JSONObject jsonObject = new JSONObject();

		try {
			jsonObject = new JSONObject(result);
			JSONArray array = jsonObject.getJSONArray("routes");
			JSONObject routes = array.getJSONObject(0);
			JSONArray legs = routes.getJSONArray("legs");
			JSONObject steps = legs.getJSONObject(0);

			// test print
			new Step(steps.getJSONArray("steps").getJSONObject(0)).printOut();

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Searches map
	 * 
	 * @param query
	 *            for this
	 */
	public void searchMap(String query) {
		System.out.println("Searching for " + query);
		try {
			final List<Address> addresses = (List<Address>) new Geocoder(this).getFromLocationName(query, 50);
			ArrayList<String> labels = new ArrayList<String>();

			for (Address ad : addresses) {
				labels.add(ad.getAddressLine(0) + " " + ad.getAddressLine(1));
			}

			ListView list = new ListView(this);
			list.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, labels));

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Results for '" + query + "':");
			builder.setView(list);
			final AlertDialog alert = builder.create();
			alert.show();

			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
					Address ad = addresses.get(position);
					LatLng point = new LatLng(ad.getLatitude(), ad.getLongitude());
					putMarker(point);
					zoomToPoint(point);
					alert.dismiss();
				}

			});

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.plan_route, menu);

		final SearchView searchView = new SearchView(getSupportActionBar().getThemedContext());
		searchView.setQueryHint("Search");

		MenuItem item = menu.findItem(R.id.menu_search);
		MenuItemCompat.setActionView(item, searchView);

		// search box
		searchView.setOnQueryTextListener(new OnQueryTextListener() {
			@Override
			public boolean onQueryTextChange(String text) {

				return true;
			}

			@Override
			public boolean onQueryTextSubmit(String text) {
				if (text.length() > 0)
					searchMap(text);
				return true;
			}
		});
		searchView.setOnCloseListener(new OnCloseListener() {

			@Override
			public boolean onClose() {
				return false;
			}

		});

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.start)
			start();
		return true;
	}
}
